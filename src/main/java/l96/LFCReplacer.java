package l96;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class LFCReplacer {
    public static void main(String[] args) {
        try {
            new LFCReplacer(args[0]);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public LFCReplacer(String str) throws IOException {
        replace(new File(System.getProperty("user.dir"), str));
    }

    public void replace(File folder) throws IOException {
        if(folder.exists()) {
            if(folder.isDirectory()) {
                for(File file: folder.listFiles()) {
                    if(file.isDirectory()) {
                        replace(file);
                    } else if(file.getName().contains("cfg")) {
                        replaceFile(file);
                    }
                }
            } else {
                replaceFile(folder);
            }
        }
    }

    private void replaceFile(File file) throws IOException {
        System.out.println(file.getName() + "\n");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        StringBuilder builder = new StringBuilder();
        String str;
        while((str = reader.readLine()) != null) {
            System.out.println("\r" + str);
            builder.append(str).append("\n");
        }
        Files.write(file.toPath(), builder.toString().getBytes(StandardCharsets.UTF_8));
    }
}
